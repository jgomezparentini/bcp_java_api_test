package test.bcp.java_api.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "moneda")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Moneda {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

    @Column(name = "moneda")
    private String moneda;
}
