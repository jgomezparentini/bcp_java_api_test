package test.bcp.java_api.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "cambios")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Cambio {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

    @Column(name = "monto")
    private long monto;

    @Column(name = "moneda_origen")
    private String moneda_origen;

    @Column(name = "moneda_destino")
    private String moneda_destino;

    @Column(name = "monto_tipo_cambio")
    private long monto_tipo_cambio;

    @Column(name = "tipo_cambio")
    private String tipo_cambio;
    
    @Column(name = "usuario")
    private String usuario;
}
