package test.bcp.java_api.Service;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.reactivex.Completable;
import io.reactivex.Single;
import test.bcp.java_api.dto.request.ActualizaCambioRequest;
import test.bcp.java_api.dto.request.RegistraCambioRequest;
import test.bcp.java_api.dto.response.CambioResponse;
import test.bcp.java_api.entity.Cambio;
import test.bcp.java_api.repository.CambioRepository;

@Service
public class CambioServiceImpl implements CambioService {

    @Autowired
    private CambioRepository cambioRepository;

    @Override
    public Single<CambioResponse> registraCambio(RegistraCambioRequest registraCambioRequest, String usuario){
        return guardarCambio(registraCambioRequest,usuario);
    };

    private Single<CambioResponse> guardarCambio(RegistraCambioRequest registraCambioRequest, String usuario){
        return Single.create(singleSubscriber ->{
            Cambio  cambio = cambioRepository.save(toCambio(registraCambioRequest,usuario)) ;

            singleSubscriber.onSuccess(toCambioResponse(cambio));
        });
    }

    private Cambio toCambio(RegistraCambioRequest registraCambioRequest, String usuario) {
        Cambio cambio = new Cambio();
        BeanUtils.copyProperties(registraCambioRequest, cambio);

        cambio.setUsuario(usuario);
        cambio.setMonto(registraCambioRequest.getMonto());
        cambio.setMoneda_origen(registraCambioRequest.getMoneda_origen());
        cambio.setMoneda_destino(registraCambioRequest.getMoneda_destino());
        cambio.setMonto_tipo_cambio(registraCambioRequest.getMonto_tipo_cambio());
        cambio.setTipo_cambio(registraCambioRequest.getTipo_cambio());
   
        return cambio;
    }

    @Override
    public Completable actualizaCambio(ActualizaCambioRequest actualizaCambioRequest, String usuario) {
        // TODO Auto-generated method stub
        return actualizaCambioBD(actualizaCambioRequest,usuario);
    }

    private Completable actualizaCambioBD(ActualizaCambioRequest actualizaCambioRequest, String usuario){
        return Completable.create(completableSubscriber -> {
            
            Optional<Cambio> optionalCambio = cambioRepository.findById(actualizaCambioRequest.getId() );
            if (!optionalCambio.isPresent())
                completableSubscriber.onError(new EntityNotFoundException());
            else {
                Cambio cambio = optionalCambio.get();
                cambio.setMonto(actualizaCambioRequest.getMonto());
                cambio.setMoneda_origen(actualizaCambioRequest.getMoneda_origen());
                cambio.setMoneda_destino(actualizaCambioRequest.getMoneda_destino());
                cambio.setMonto_tipo_cambio(actualizaCambioRequest.getMonto_tipo_cambio());
                cambio.setTipo_cambio(actualizaCambioRequest.getTipo_cambio());
                cambio.setUsuario(usuario);
                cambioRepository.save(cambio);
                completableSubscriber.onComplete();
            }
            }
        );
    }

    @Override
    public Single<CambioResponse> obtieneTipoCambio(long id) {
        // TODO Auto-generated method stub
        return buscaCambioEnBD(id);
    }

    private Single<CambioResponse> buscaCambioEnBD(long id){
        return Single.create(singleSubscriber -> {
            Optional<Cambio> optionalCambio = cambioRepository.findById(id);
            if (!optionalCambio.isPresent())
                singleSubscriber.onError(new EntityNotFoundException());
            else {
                CambioResponse cambioResponse = toCambioResponse(optionalCambio.get());
                singleSubscriber.onSuccess(cambioResponse);
            }
        });
    }

    private CambioResponse toCambioResponse(Cambio cambio) {
        CambioResponse cambioResponse = new CambioResponse();
        BeanUtils.copyProperties(cambio, cambioResponse);
        
        cambioResponse.setMonto(cambio.getMonto());
        cambioResponse.setMoneda_destino(cambio.getMoneda_destino());
        cambioResponse.setMoneda_origen(cambio.getMoneda_origen());
        cambioResponse.setMonto_tipo_cambio(cambio.getMonto_tipo_cambio());
        cambioResponse.setTipo_cambio(cambio.getTipo_cambio());
        cambioResponse.setUsuario(cambio.getUsuario());
     
        return cambioResponse;
    }
    
}
