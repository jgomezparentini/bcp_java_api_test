package test.bcp.java_api.Service;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import io.reactivex.Single;
import test.bcp.java_api.dto.request.LoginRequest;
import test.bcp.java_api.dto.response.AuthResponse;
import test.bcp.java_api.entity.Usuario;
import test.bcp.java_api.repository.AuthRepository;

@Service
public class AuthServiceImpl implements AuthService{

    @Autowired
    private AuthRepository authRepository;

    @Override
    public Single<AuthResponse> validaUsuario(LoginRequest loginRequest, String token) {
        return validar(loginRequest,token);
    }

    private Single<AuthResponse> validar(LoginRequest loginRequest, String token) {
        String usuario = loginRequest.getUsuario();

        Usuario user = new Usuario();
        user.setUsuario(loginRequest.getUsuario());
        user.setPassword(loginRequest.getPassword());

        ExampleMatcher modelMatcher = ExampleMatcher.matching().withIgnorePaths("id");
        Example<Usuario> example = Example.of(user, modelMatcher);

        return Single.create(singleSubscriber -> {
        
            if (!authRepository.exists(example))
                singleSubscriber.onError(new EntityNotFoundException());
            else {
                AuthResponse authResponse = toAuthResponse(usuario,token);
                singleSubscriber.onSuccess(authResponse);
            }
        });

    }

    private AuthResponse toAuthResponse(String usuario, String token) {
        AuthResponse authResponse = new AuthResponse();
          
        authResponse.setUsuario(usuario);
        authResponse.setToken(token);
     
        return authResponse;
    }
    
    
}
