package test.bcp.java_api.Service;

import io.reactivex.Single;
import test.bcp.java_api.dto.request.LoginRequest;
import test.bcp.java_api.dto.response.AuthResponse;


public interface AuthService {
    Single<AuthResponse> validaUsuario(LoginRequest loginRequest, String token);
}
