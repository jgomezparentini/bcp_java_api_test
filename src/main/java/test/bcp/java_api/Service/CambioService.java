package test.bcp.java_api.Service;

import io.reactivex.Completable;
import io.reactivex.Single;
import test.bcp.java_api.dto.request.ActualizaCambioRequest;
import test.bcp.java_api.dto.request.RegistraCambioRequest;
import test.bcp.java_api.dto.response.CambioResponse;

public interface CambioService {
    Single<CambioResponse> registraCambio(RegistraCambioRequest registraCambioRequest, String usuario);

    Completable actualizaCambio(ActualizaCambioRequest actualizaCambioRequest, String usuario);

    Single<CambioResponse> obtieneTipoCambio(long id);
}
