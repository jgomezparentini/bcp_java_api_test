package test.bcp.java_api.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegistraCambioRequest {
    private long monto;
    private String moneda_origen;
    private String moneda_destino;
    private long monto_tipo_cambio;
    private String tipo_cambio;
}
