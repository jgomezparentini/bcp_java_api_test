package test.bcp.java_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.bcp.java_api.entity.Usuario;

@Repository
public interface AuthRepository extends JpaRepository<Usuario, String>{
    
}
