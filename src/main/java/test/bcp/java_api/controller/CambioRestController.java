package test.bcp.java_api.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import test.bcp.java_api.Service.CambioService;
import test.bcp.java_api.dto.request.ActualizaCambioRequest;
import test.bcp.java_api.dto.request.RegistraCambioRequest;
import test.bcp.java_api.dto.response.BaseWebResponse;
import test.bcp.java_api.dto.response.CambioResponse;

@RestController
@RequestMapping(value = "/api/cambio")
public class CambioRestController {
    
    private final String HEADER = "Authorization";
	private final String PREFIX = "Bearer ";
    private final String SECRET = "mySecretKey";

    @Autowired
    private CambioService cambioService;

    @PostMapping(
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
        )
    public Single<ResponseEntity<BaseWebResponse<CambioResponse>>> agregaCambio(
            @RequestBody RegistraCambioRequest registraCambioRequest,
            @RequestHeader(HEADER) String token
            ) {
           
                String usuario = getUsuarioToken(token);
                return cambioService.registraCambio(registraCambioRequest,usuario).subscribeOn(Schedulers.io()).map(
                s -> ResponseEntity.created(URI.create("/api/cambio" + s))
                        .body(BaseWebResponse.successWithData(s)));
               
    }

    private String getUsuarioToken(String token){
        String jwtToken = token.replace(PREFIX, "");
        Claims jwtBody = Jwts.parser().setSigningKey(SECRET.getBytes()).parseClaimsJws(jwtToken).getBody();

        return jwtBody.getSubject();
    }
    
    @PutMapping(
        value = "/{cambioId}",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
        )
    public Single<ResponseEntity<BaseWebResponse>> actualizaCambio(@PathVariable(value = "cambioId") long cambioId,
            @RequestBody ActualizaCambioRequest actualizaCambioWebRequest,
            @RequestHeader(HEADER) String token
            ) {
                String usuario = getUsuarioToken(token);
           
                return cambioService.actualizaCambio(actualizaCambioRequest(cambioId, actualizaCambioWebRequest),usuario)
                .subscribeOn(Schedulers.io())
                .toSingle(() -> ResponseEntity.ok(BaseWebResponse.successNoData()));
    }

    private ActualizaCambioRequest actualizaCambioRequest(long cambioId, ActualizaCambioRequest actualizaCambioWebRequest) {

        actualizaCambioWebRequest.setId(cambioId);
        return actualizaCambioWebRequest;
    }

    @GetMapping(
        value = "/{cambioId}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Single<ResponseEntity<BaseWebResponse<CambioResponse>>> obtieneCambio(
        @PathVariable(value = "cambioId") long cambioId,
        @RequestHeader(HEADER) String token
        ) {

    return cambioService.obtieneTipoCambio(cambioId)
            .subscribeOn(Schedulers.io())
            .map(cambioResponse -> ResponseEntity.ok(BaseWebResponse.successWithData(cambioResponse)));
}

}
