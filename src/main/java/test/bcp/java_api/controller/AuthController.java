package test.bcp.java_api.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import test.bcp.java_api.Service.AuthService;
import test.bcp.java_api.dto.request.LoginRequest;
import test.bcp.java_api.dto.response.AuthResponse;
import test.bcp.java_api.dto.response.BaseWebResponse;

@RestController
@RequestMapping(value = "/auth")
public class AuthController {

	@Autowired
    private AuthService authService;

    @PostMapping(
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
        )
	public Single<ResponseEntity<BaseWebResponse<AuthResponse>>> login(@RequestBody Map<String, String> body) {
		
		String username = body.get("user");
		String password = body.get("password");
		String token = getJWTToken(username);
		
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsuario(username);
		loginRequest.setPassword(password);	
		
		return authService.validaUsuario(loginRequest,token)
		.subscribeOn(Schedulers.io())
		.map(authResponse -> ResponseEntity.ok(BaseWebResponse.successWithData(authResponse)));

	}

	private String getJWTToken(String username) {
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		String token = Jwts
				.builder()
				.setId("jgomez")
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 6000000))
				.signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();

		return "Bearer " + token;
	}
    
}
