DROP TABLE IF EXISTS cambios;
CREATE TABLE cambios(
    id serial PRIMARY KEY,
    monto integer,
    moneda_origen VARCHAR(255) NOT NULL,
    moneda_destino VARCHAR(255) NOT NULL,
    monto_tipo_cambio integer,
    tipo_cambio VARCHAR(255) NOT NULL,
    usuario VARCHAR(255) NOT NULL
    );

DROP TABLE IF EXISTS usuarios;
CREATE TABLE usuarios(
    usuario VARCHAR(255) PRIMARY KEY NOT NULL,
    password VARCHAR(255) NOT NULL
    );

DROP TABLE IF EXISTS moneda;
CREATE TABLE moneda(
    id serial PRIMARY KEY,
    moneda VARCHAR(255) NOT NULL
    );
