# API Test BCP #

Resumen de funcionamiento de API Test de BCP

### Para utilizar la API ###

Cada vez que se levante la API se recrea el modelo de datos segun la definicion de los scripts de la carpeta resorces.

En la base de dato es un servicio postgress que corre en la nube, segun la configuracion del archivo application.properties

La API cuenta con su documentacion en la ruta http://localhost:8080/swagger-ui.html

Los tokens tienen un tiempo de vida de 10 minutos, segun lo especificado en AuthController.java

